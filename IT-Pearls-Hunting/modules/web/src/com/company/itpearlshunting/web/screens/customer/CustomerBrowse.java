package com.company.itpearlshunting.web.screens.customer;

import com.haulmont.cuba.gui.screen.*;

@UiController("itpearlshunting_Customer.browse")
@UiDescriptor("customer-browse.xml")
@LookupComponent("customersTable")
@LoadDataBeforeShow
public class CustomerBrowse extends StandardLookup<Customer> {
}