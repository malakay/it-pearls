package com.company.itpearlshunting.web.screens.typeofproperty;

import com.haulmont.cuba.gui.screen.*;
import com.company.itpearlshunting.entity.TypeOfProperty;

@UiController("itpearlshunting_TypeOfProperty.browse")
@UiDescriptor("type-of-property-browse.xml")
@LookupComponent("typeOfPropertiesTable")
@LoadDataBeforeShow
public class TypeOfPropertyBrowse extends StandardLookup<TypeOfProperty> {
}