package com.company.itpearlshunting.web.screens.customer;

import com.haulmont.cuba.gui.screen.*;

@UiController("itpearlshunting_Customer.edit")
@UiDescriptor("customer-edit.xml")
@EditedEntityContainer("customerDc")
@LoadDataBeforeShow
public class CustomerEdit extends StandardEditor<Customer> {
}