package com.company.itpearlshunting.web.screens.typeofproperty;

import com.haulmont.cuba.gui.screen.*;
import com.company.itpearlshunting.entity.TypeOfProperty;

@UiController("itpearlshunting_TypeOfProperty.edit")
@UiDescriptor("type-of-property-edit.xml")
@EditedEntityContainer("typeOfPropertyDc")
@LoadDataBeforeShow
public class TypeOfPropertyEdit extends StandardEditor<TypeOfProperty> {
}