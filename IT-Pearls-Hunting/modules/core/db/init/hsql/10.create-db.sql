-- begin ITPEARLSHUNTING_TYPE_OF_PROPERTY
create table ITPEARLSHUNTING_TYPE_OF_PROPERTY (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TYPE_ varchar(80) not null,
    ABBREVIATED varchar(7) not null,
    --
    primary key (ID)
)^
-- end ITPEARLSHUNTING_TYPE_OF_PROPERTY
-- begin ITPEARLSHUNTING_COMPANY
create table ITPEARLSHUNTING_COMPANY (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    COMPANY_NAME varchar(255) not null,
    LEGAL_ENTITY varchar(256) not null,
    --
    primary key (ID)
)^
-- end ITPEARLSHUNTING_COMPANY
