package com.company.itpearlshunting.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "ITPEARLSHUNTING_TYPE_OF_PROPERTY")
@Entity(name = "itpearlshunting_TypeOfProperty")
public class TypeOfProperty extends StandardEntity {
    private static final long serialVersionUID = -556493657762947032L;

    @NotNull
    @Column(name = "TYPE_", nullable = false, unique = true, length = 80)
    protected String type;

    @NotNull
    @Column(name = "ABBREVIATED", nullable = false, unique = true, length = 7)
    protected String abbreviated;

    public String getAbbreviated() {
        return abbreviated;
    }

    public void setAbbreviated(String abbreviated) {
        this.abbreviated = abbreviated;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}