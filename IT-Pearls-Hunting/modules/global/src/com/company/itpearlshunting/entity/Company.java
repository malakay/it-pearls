package com.company.itpearlshunting.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "ITPEARLSHUNTING_COMPANY")
@Entity(name = "itpearlshunting_Company")
public class Company extends StandardEntity {
    private static final long serialVersionUID = -2776509784584453448L;

    @NotNull
    @Column(name = "COMPANY_NAME", nullable = false, unique = true)
    protected String companyName;

    @NotNull
    @Column(name = "LEGAL_ENTITY", nullable = false, length = 256)
    protected String legalEntity;

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}