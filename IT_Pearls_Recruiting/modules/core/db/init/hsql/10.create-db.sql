-- begin ITPEARLSRECRUITING_FORM_OF_ORGANISATION
create table ITPEARLSRECRUITING_FORM_OF_ORGANISATION (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LONG_NAME varchar(50) not null,
    SHORT_NAME varchar(7) not null,
    --
    primary key (ID)
)^
-- end ITPEARLSRECRUITING_FORM_OF_ORGANISATION
-- begin ITPEARLSRECRUITING_PERSON
create table ITPEARLSRECRUITING_PERSON (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    POSITION_ID varchar(36),
    FIRST_NAME varchar(30) not null,
    MIDDLE_NAME varchar(30),
    SECOND_NAME varchar(30),
    PERSON_COUNTRY_ID varchar(36),
    PHONE_NUMBER integer,
    EMAIL varchar(30),
    SKYPE_NAME varchar(30),
    TELEGRAM_NAME varchar(30),
    CANDIDATE boolean,
    --
    primary key (ID)
)^
-- end ITPEARLSRECRUITING_PERSON
-- begin ITPEARLSRECRUITING_LEGAL_ENTITY
create table ITPEARLSRECRUITING_LEGAL_ENTITY (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LEGAL_NAME varchar(255) not null,
    COMPANY_ADDRESS varchar(255),
    COMPANY_PHONE varchar(10),
    COMPANY_FAX varchar(10),
    --
    primary key (ID)
)^
-- end ITPEARLSRECRUITING_LEGAL_ENTITY
-- begin ITPEARLSRECRUITING_POSITION
create table ITPEARLSRECRUITING_POSITION (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    POSITION_LOCAL_NAME varchar(80) not null,
    POSITION_NAME varchar(80),
    --
    primary key (ID)
)^
-- end ITPEARLSRECRUITING_POSITION
