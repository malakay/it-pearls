create table ITPEARLSRECRUITING_POSITION (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    POSITION_LOCAL_NAME varchar(80) not null,
    POSITION_NAME varchar(80),
    --
    primary key (ID)
);