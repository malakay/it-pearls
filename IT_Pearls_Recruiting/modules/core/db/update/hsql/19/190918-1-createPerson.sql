create table ITPEARLSRECRUITING_PERSON (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(30) not null,
    MIDDLE_NAME varchar(30),
    SECOND_NAME varchar(30),
    PHONE_NUMBER integer,
    EMAIL varchar(30),
    SKYPE_NAME varchar(30),
    TELEGRAM_NAME varchar(30),
    CANDIDATE boolean,
    --
    primary key (ID)
);