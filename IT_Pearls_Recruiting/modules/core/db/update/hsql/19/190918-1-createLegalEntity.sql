create table ITPEARLSRECRUITING_LEGAL_ENTITY (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LEGAL_NAME varchar(255) not null,
    COMPANY_ADDRESS varchar(255),
    COMPANY_PHONE integer,
    COMPANY_FAX integer,
    --
    primary key (ID)
);