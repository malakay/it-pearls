create table ITPEARLSRECRUITING_COUNRIES (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    COUNTRY_NAME varchar(50) not null,
    COUNTRY_SHORT_NAME varchar(2),
    COUNTRY_PHONE_CODE varchar(5),
    --
    primary key (ID)
);