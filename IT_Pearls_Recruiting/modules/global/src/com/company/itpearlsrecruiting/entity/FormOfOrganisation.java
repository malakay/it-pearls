package com.company.itpearlsrecruiting.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.CaseConversion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "ITPEARLSRECRUITING_FORM_OF_ORGANISATION")
@Entity(name = "itpearlsrecruiting_FormOfOrganisation")
public class FormOfOrganisation extends StandardEntity {
    private static final long serialVersionUID = 7411365566207103293L;

    @NotNull
    @Column(name = "LONG_NAME", nullable = false, unique = true, length = 50)
    protected String longName;

    @CaseConversion
    @NotNull
    @Column(name = "SHORT_NAME", nullable = false, unique = true, length = 7)
    protected String shortName;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }
}