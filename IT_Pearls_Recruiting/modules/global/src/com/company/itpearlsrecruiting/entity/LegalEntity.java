package com.company.itpearlsrecruiting.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "ITPEARLSRECRUITING_LEGAL_ENTITY")
@Entity(name = "itpearlsrecruiting_LegalEntity")
public class LegalEntity extends StandardEntity {
    private static final long serialVersionUID = -5341068197516580820L;

    @NotNull
    @Column(name = "LEGAL_NAME", nullable = false)
    protected String legalName;

    @Column(name = "COMPANY_ADDRESS")
    protected String companyAddress;

    @Column(name = "COMPANY_PHONE", length = 10)
    protected String companyPhone;

    @Column(name = "COMPANY_FAX", length = 10)
    protected String companyFax;

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyFax(String companyFax) {
        this.companyFax = companyFax;
    }

    public String getCompanyFax() {
        return companyFax;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }
}