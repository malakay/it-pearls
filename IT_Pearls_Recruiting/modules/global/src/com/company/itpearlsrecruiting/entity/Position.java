package com.company.itpearlsrecruiting.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamePattern("%s %s|positionLocalName,positionName")
@Table(name = "ITPEARLSRECRUITING_POSITION")
@Entity(name = "itpearlsrecruiting_Position")
public class Position extends StandardEntity {
    private static final long serialVersionUID = 3135825904295353547L;

    @NotNull
    @Column(name = "POSITION_LOCAL_NAME", nullable = false, length = 80)
    protected String positionLocalName;

    @Column(name = "POSITION_NAME", length = 80)
    protected String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionLocalName() {
        return positionLocalName;
    }

    public void setPositionLocalName(String positionLocalName) {
        this.positionLocalName = positionLocalName;
    }
}