package com.company.itpearlsrecruiting.entity;

import com.haulmont.chile.core.annotations.NumberFormat;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.CaseConversion;
import com.haulmont.cuba.core.entity.annotation.ConversionType;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Table(name = "ITPEARLSRECRUITING_PERSON")
@Entity(name = "itpearlsrecruiting_Person")
public class Person extends StandardEntity {
    private static final long serialVersionUID = -4216364959025919373L;

    @Lookup(type = LookupType.DROPDOWN, actions = {"lookup"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POSITION_ID")
    protected Position position;

    @NotNull
    @Column(name = "FIRST_NAME", nullable = false, length = 30)
    protected String firstName;

    @Column(name = "MIDDLE_NAME", length = 30)
    protected String middleName;

    @Column(name = "SECOND_NAME", length = 30)
    protected String secondName;

    @Lookup(type = LookupType.DROPDOWN, actions = {"lookup"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_COUNTRY_ID")
    protected Counries personCountry;

    @NumberFormat(pattern = "(XXX) XXXX-XX-XX")
    @Column(name = "PHONE_NUMBER")
    protected Integer phoneNumber;

    @Email(regexp = "Email in not valid")
    @CaseConversion(type = ConversionType.LOWER)
    @Column(name = "EMAIL", length = 30)
    protected String email;

    @CaseConversion(type = ConversionType.LOWER)
    @Column(name = "SKYPE_NAME", length = 30)
    protected String skypeName;

    @CaseConversion(type = ConversionType.LOWER)
    @Column(name = "TELEGRAM_NAME", length = 30)
    protected String telegramName;

    @Column(name = "CANDIDATE")
    protected Boolean candidate;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Counries getPersonCountry() {
        return personCountry;
    }

    public void setPersonCountry(Counries personCountry) {
        this.personCountry = personCountry;
    }

    public Boolean getCandidate() {
        return candidate;
    }

    public void setCandidate(Boolean candidate) {
        this.candidate = candidate;
    }

    public String getTelegramName() {
        return telegramName;
    }

    public void setTelegramName(String telegramName) {
        this.telegramName = telegramName;
    }

    public String getSkypeName() {
        return skypeName;
    }

    public void setSkypeName(String skypeName) {
        this.skypeName = skypeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}