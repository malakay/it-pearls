package com.company.itpearlsrecruiting.web.screens.formoforganisation;

import com.haulmont.cuba.gui.screen.*;
import com.company.itpearlsrecruiting.entity.FormOfOrganisation;

@UiController("itpearlsrecruiting_FormOfOrganisation.edit")
@UiDescriptor("form-of-organisation-edit.xml")
@EditedEntityContainer("formOfOrganisationDc")
@LoadDataBeforeShow
public class FormOfOrganisationEdit extends StandardEditor<FormOfOrganisation> {
}