package com.company.itpearlsrecruiting.web.screens.formoforganisation;

import com.haulmont.cuba.gui.screen.*;
import com.company.itpearlsrecruiting.entity.FormOfOrganisation;

@UiController("itpearlsrecruiting_FormOfOrganisation.browse")
@UiDescriptor("form-of-organisation-browse.xml")
@LookupComponent("formOfOrganisationsTable")
@LoadDataBeforeShow
public class FormOfOrganisationBrowse extends StandardLookup<FormOfOrganisation> {
}